FROM python:3.11

RUN groupadd -g 1000 python
RUN useradd -ms /bin/bash -u 1000 -g 1000 python
USER python

ENV PATH $PATH:/home/python/.local/bin

RUN python -m pip install --upgrade pip

WORKDIR /app
COPY ./app .
RUN pip install --no-cache-dir -r configs/requirements.txt

CMD [ "bash" ]
