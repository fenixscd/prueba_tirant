import sys
import re
import unicodedata
from bs4 import SoupStrainer, BeautifulSoup
sys.path.append("/app")
from configs.environment import Environment
from configs.logger import logger
from infra.conection import Conection

class PageContracts():
        
    @staticmethod
    def all_for_url(url):
        page_contracts = PageContracts()
        contract = page_contracts._all_for_url(url)
        return contract

    def _all_for_url(self, url):
        all_contracts = []

        while Conection.is_valid_url(url):
            text_page = Conection.get_text_page(url)
            contracts = self._extract_contracts(text_page)
            all_contracts.extend(contracts)
            url = self._extract_next_url(text_page)
        return all_contracts

    def _extract_contracts(self, text_page):
        total_contracts = []
        soup_contracts = self._get_soup_contracts(text_page)

        for soup_contract in soup_contracts:
            data = self._extract_contract(soup_contract)
            total_contracts.append(data) 
        return total_contracts

    def _get_soup_contracts(self, text_page):
        soup_strainer = SoupStrainer('div', class_="portlet-body")
        soup = BeautifulSoup(text_page, 'html.parser', parse_only=soup_strainer)
        contracts = soup.find_all('div', class_='no-title')
        return contracts

    def _extract_next_url(self, text_page):
        soup_strainer = SoupStrainer('ul', class_="lfr-pagination-buttons pager")
        soup = BeautifulSoup(text_page, 'html.parser', parse_only=soup_strainer)
        
        next_link = soup.find('a', string=re.compile("Siguiente"))
        url = ''

        if next_link != None:
            url = next_link['href']
        return url
        
    def _extract_contract(self, soup_contract):
        title = self._extract_title(soup_contract)
        date = self._extract_date(soup_contract)
        return {'title':title, 'date':date}

    def _extract_title(self, soup_contract):
        dirty_title = soup_contract.find('h2').text
        return self._clean_text(dirty_title)
    
    def _extract_date(self, soup_contract):
        date_expression = "\d{1,2}\/\d{1,2}\/\d{2}"
        date_soup = soup_contract.find('input', value=re.compile(date_expression))
        return date_soup['value']

    def _clean_text(self, text):
        text_without_break_lines = self._remove_break_lines(text)
        result = self._remove_duplicate_spaces(text_without_break_lines)
        normalized_text = unicodedata.normalize('NFC', result)
        return normalized_text

    def _remove_break_lines(self, text):
        return text.replace("\n", "")

    def _remove_duplicate_spaces(self,text):
        texto = text.replace('\xa0', '')
        texto = texto.replace("  ", " ")
        texto = texto.replace("  ", " ")
        return texto
