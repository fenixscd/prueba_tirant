import sys
sys.path.append("/app")
from configs.logger import logger

from src.services.contracts.page_contracts import PageContracts
from src.domain.domain import Domain

class Contracts():
    def all_for_catalog(section):
        url = section.get_url()
        list_contracts = PageContracts.all_for_url(url)
        return Domain.Contracts.from_dictionary(list_contracts)