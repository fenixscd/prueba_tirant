import sys
sys.path.append("/app")
from configs.logger import logger

from src.services.catalogs.page_catalogs import Catalogs
from src.domain.domain import Domain

class Catalog():
    def for_title(catalog):
        title = catalog
        url = Catalogs.find_url(title)
        logger.info(url)
        dictionary = {'title': title, 'url':url}
        catalog = Domain.Catalog.from_dictionary(dictionary)
        return catalog
