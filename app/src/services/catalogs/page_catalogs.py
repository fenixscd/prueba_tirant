import os
from bs4 import BeautifulSoup, SoupStrainer
import sys
sys.path.append("/app")
from configs.logger import logger
from configs.environment import Environment
from infra.conection import Conection

class Catalogs():

    @staticmethod
    def find_url(title):
        page_text = Catalogs._get_text_page()
        links = SoupStrainer('a', href=True)
        soup = BeautifulSoup(page_text, 'html.parser', parse_only=links)
        link = soup.find('a', string=title)
        if link == None:
            logger.error(f"URL not found on the indicated page title: {title}")
            return ''
        route = link['href']
        return Environment.get_domain() + route

    def _get_text_page():
        url = None
        mode = os.environ.get('MODE', 'test')
        if mode == 'production':
            url = Environment.URL_INITIAL_PAGE
        else:
            url = Environment.URL_INITIAL_PAGE_TESTS
        return Conection.get_text_page(url)

    