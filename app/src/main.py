import json
import sys
sys.path.append("/app")

from configs.logger import logger
from src.actions.actions import Actions


def main():
    catalog_name = 'Acuerdos año 2020'
    contracts_list = Actions.ExtractContractsForCatalogName.do(catalog_name)

    contracts_dictionary = {"Acuerdos año 2020": contracts_list}
    contracts_json = json.dumps(contracts_dictionary, ensure_ascii=False)

    print(contracts_json)

    with open('datos.json', 'w') as file:
        file.write(contracts_json)

logger.info("START app ...")

main()

logger.info("... FINISHED app")


