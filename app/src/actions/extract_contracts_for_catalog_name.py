import sys
sys.path.append("/app")

from configs.logger import logger
from src.services.service import Service

class ExtractContractsForCatalogName():
    @staticmethod
    def do(catalog_name):
        logger.info(f"ExtractContractsForCatalogName: {catalog_name}")
        catalog = Service.Catalog.for_title(catalog_name)
        contracts = Service.Contracts.all_for_catalog(catalog)
        
        return contracts.get_list_contracts()

