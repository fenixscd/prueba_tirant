import sys
sys.path.append("/app")
from configs.logger import logger
from src.domain.contract import Contract

class Contracts():
    def __init__(self):
        self.contracts = []
    
    def add(self, contract):
        self.contracts.append(contract)

    def total(self):
        return len(self.contracts)
    
    @staticmethod
    def from_dictionary(dictionary):
        contracts = Contracts()
        for contract in dictionary:
            contracts.add(Contract(contract['title'], contract['date']))
        return contracts

    def get_list_contracts(self):
        result = []

        for contract in self.contracts:
            result.append(contract.get_dictionary())
            
        return result
