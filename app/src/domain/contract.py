import sys
sys.path.append("/app")
from configs.logger import logger

class Contract():

    def __init__(self, title, date):
        self.title = title
        self.date = date
    
    def get_dictionary(self):
        return {"titulo": self.title, "fecha": self.date}