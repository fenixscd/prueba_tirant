import sys
sys.path.append("/app")

from src.domain.contract import Contract
from src.domain.contracts import Contracts
from src.domain.catalog import Catalog

class Domain():
    Contract = Contract
    Contracts = Contracts
    Catalog = Catalog
