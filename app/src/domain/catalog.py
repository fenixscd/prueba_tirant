import sys
sys.path.append("/app")
from configs.logger import logger

class Catalog():

    def __init__(self, title):
        self.title = title
        self.url = ''
    
    def get_dictionary(self):
        return {'title': self.title, 'url': self.url}

    def set_url(self, url):
        self.url = url
    
    def get_url(self):
        return self.url

    @staticmethod
    def from_dictionary(dictionary):
        catalog = Catalog(dictionary['title'])
        catalog.set_url(dictionary['url'])
        return catalog