import unittest
import sys
sys.path.append("/app")
from configs.logger import logger 
from configs.logger import file_handler 

# point the discover to the path where you have your tests
test_suite = unittest.defaultTestLoader.discover("/app/tests/", pattern="test_*.py")

# For not save logs at logs file
logger.handlers.remove(file_handler)

# run the tests
unittest.TextTestRunner().run(test_suite)