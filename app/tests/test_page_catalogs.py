import unittest
import sys
sys.path.append("/app")

from src.services.catalogs.page_catalogs import Catalogs
from configs.environment import Environment

class TestPageCatalogs(unittest.TestCase):
    def test_it_returns_specified_url_according_to_title(self):
        title_url = "Acuerdos año 2020"
        url  = Catalogs.find_url(title_url)

        expected = Environment.URL_CONTRACTS_PAGE_TESTS
        self.assertEqual(expected, url)

    def test_it_does_not_return_specified_url_according_to_title(self):
        title_url = "catalog not exist"
        url  = Catalogs.find_url(title_url)

        expected = ''
        self.assertEqual(expected, url)
