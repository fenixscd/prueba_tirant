import unittest
import sys
sys.path.append("/app")

from src.domain.domain import Domain

class TestDomeinContracts(unittest.TestCase):
    def test_it_returns_dictionary_contracts(self):

        one_contract = Domain.Contract('one title', '1/1/20')
        two_contract = Domain.Contract('two title', '2/2/20')
        thre_contract = Domain.Contract('thre title', '3/3/20')

        contracts = Domain.Contracts()
        contracts.add(one_contract)
        contracts.add(two_contract)
        contracts.add(thre_contract)

        list_contracts = contracts.get_list_contracts()

        expected = [{'titulo': 'one title', 'fecha': '1/1/20'}, 
                    {'titulo': 'two title', 'fecha': '2/2/20'}, 
                    {'titulo': 'thre title', 'fecha': '3/3/20'}]
        self.assertEqual(expected, list_contracts)
