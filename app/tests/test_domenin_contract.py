import unittest
import sys
sys.path.append("/app")

from src.domain.domain import Domain

class TestDomeinContract(unittest.TestCase):
    def test_it_returns_dictionary(self):

        title = 'a title'
        date = '1/1/20'
        contract = Domain.Contract(title, date)

        dictionary = contract.get_dictionary()
        
        expected = {"titulo":'a title',"fecha": '1/1/20'}
        self.assertEqual(expected, dictionary)
