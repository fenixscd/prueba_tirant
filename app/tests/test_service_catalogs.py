import unittest
import sys
sys.path.append("/app")

from src.services.service import Service
from configs.environment import Environment

class TestServiceCatalogs(unittest.TestCase):
    def test_it_return_catalog_from_title(self):
        section = 'Acuerdos año 2020'

        catalog = Service.Catalog.for_title(section)

        expected = {'title': 'Acuerdos año 2020', 'url': Environment.URL_CONTRACTS_PAGE_TESTS}
        self.assertEqual(expected, catalog.get_dictionary())
