import unittest
import sys
sys.path.append("/app")

from src.services.contracts.page_contracts import PageContracts
from configs.environment import Environment

class TestPageContracts(unittest.TestCase):
    def test_it_returns_all_contracts_specified_for_url(self):
        url = Environment.URL_CONTRACTS_PAGE_TESTS
        
        contracts = PageContracts.all_for_url(url)
        total = len(contracts)
        
        expected = 36
        self.assertEqual(expected, total)

    def test_it_not_returns_contract_when_url_is_not_valid(self):
        url = "url not valid"
        
        contracts  = PageContracts.all_for_url(url)
        total = len(contracts)

        expected = 0
        self.assertEqual(expected, total)
