import unittest
import sys
sys.path.append("/app")

from src.actions.actions import Actions

class TestActionExtractContractsForCatalogName(unittest.TestCase):
    def test_it_returns_adictionary_with_contractas_from_names_catalog(self):
        Catalog_name = 'Acuerdos año 2020'
        result = Actions.ExtractContractsForCatalogName.do(Catalog_name)

        total  = len(result)

        expected = 36
        self.assertEqual(expected, total)
