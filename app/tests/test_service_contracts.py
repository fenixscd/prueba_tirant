import unittest
import sys
sys.path.append("/app")

from configs.environment import Environment
from src.services.service import Service
from src.domain.domain import Domain

class TestServiceContracts(unittest.TestCase):
    def test_it_returns_all_contracts_from_catalog(self):

        catalog = Domain.Catalog('Acuerdos 2020')
        catalog.set_url(Environment.URL_CONTRACTS_PAGE_TESTS)
        
        contracts = Service.Contracts.all_for_catalog(catalog)

        total_contracts = 36
        self.assertEqual(total_contracts, contracts.total())
