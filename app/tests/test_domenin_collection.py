import unittest
import sys
sys.path.append("/app")

from src.domain.domain import Domain

class TestDomeinCollection(unittest.TestCase):
    def test_it_returns_dictionary(self):
        collection = Domain.Catalog('One title')
        collection.set_url('https://url')

        dictionary = collection.get_dictionary()
        
        expected = {"title": 'One title', "url":'https://url'}
        self.assertEqual(expected, dictionary)
