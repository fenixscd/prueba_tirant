import requests
import os

import sys
sys.path.append("/app")
from configs.logger import logger
from configs.environment import Environment

class Conection():
    @staticmethod
    def get_text_page(url):
        logger.info(f"Visit page - {url}")
        mode = os.environ.get('MODE', 'test')
        if mode == 'production':
            return Conection._https(url)
        else:
            return Conection._file(url)

    @staticmethod
    def _https(url):
        response = requests.get(url)
        if response.status_code == 200:
            return response.text
        else:
            logger.error(f"Invalid page - {url}")
            return ''

    @staticmethod
    def _file(url):
        try:
            with open(url, "r") as file:
                return file.read()
        except FileNotFoundError as e:
            logger.error(f"No such file or directory: - {url}")
            return ''

    @staticmethod
    def is_valid_url(url):
        mode = os.environ.get('MODE', 'test')
        if url == None: return False
        if mode == 'production':
            return  url.__contains__(Environment.get_domain())
        else:
            return url.__contains__(Environment.get_domain())
