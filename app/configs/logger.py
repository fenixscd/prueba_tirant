import logging


logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)

file_handler = logging.FileHandler("app.log")
# file_handler.setLevel(logging.INFO)

formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
console_formatter = logging.Formatter("%(levelname)s - %(pathname)s : %(lineno)d \n \t %(funcName)s() \n \t %(message)s\n")

console_handler.setFormatter(console_formatter)
logger.addHandler(console_handler)