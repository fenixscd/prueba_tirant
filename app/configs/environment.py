import os

class Environment():
    DOMAIN = 'https://portalcontratacion.navarra.es/es/'
    URL_INITIAL_PAGE = DOMAIN + 'tribunal-administrativo-de-contratos-publicos-de-navarra'
    
    DOMAIN_TESTS = '/app/tests/samples/'
    URL_INITIAL_PAGE_TESTS = DOMAIN_TESTS + 'catalog_contracts.txt'
    URL_CONTRACTS_PAGE_TESTS = DOMAIN_TESTS + 'contracts1.txt'

    @staticmethod
    def get_domain():
        mode = os.environ.get('MODE', 'test')
        if mode == 'production':
            return  Environment.DOMAIN
        else:
            return Environment.DOMAIN_TESTS
