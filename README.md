# Prueba técnica de Tirant lo Blanch

- Realizar el escrapeo a la página web indicada y obtener todos los contratos del año 2020.

- Para la realización de esta prueba he optado por utilizar dos de las herramientas propuestas como son **Request** y **Beautifulsoup**. 
Request la he utilizado para realizar las peticiones https y beautifulsoup para procesar el resultado obtenido.
A parte de estas dos librerías, y otras necesarias, he utilizado **Unittest** para la realización de pruebas.
Dada la importancia que se le dio en la primera entrevista al monitoreo de las aplicaciones, he considerado importante la utilización de **Logging** para poder tener reportes de lo que sucede y poder actuar en caso de cambios en la fuente de datos para evitar que se provoquen incoherencias en los mismos. 

- Otra tecnología que he añadido ha sido **Docker** con una pequeña configuración en un dockerfile, para evitar problemas entre diferentes versiones de python.


## System requirements

- Docker 20.10 or compatible

## How to build the project

```Dockerfile
docker build -t pruebatecnica .
```

## How to execute the project

```Dockerfile
docker run -it --rm -e MODE=production -v "$PWD"/app:/app pruebatecnica python src/main.py
```

## How to run the tests

```Dockerfile
docker run -it --rm -e MODE=test -v "$PWD"/app:/app pruebatecnica python tests/execute_tests.py
```


# Proceso de desarrollo

La primera toma de contacto ha sido el pdf con lo que hay que realizar. Después de googlear un rato encontré  una charla del [T3chFest 2018 - Scraping épico para gente sin APIs - Ulises Gascón](https://www.youtube.com/watch?v=-Gtv6BMvcgM) la cual me dio unas ideas generales de como funciona el webscraping y con qué herramientas se puede realizar.
Debido a que el lenguaje de programación escogido es python, las herramientas de las que se dispone para hacer este tipo de tareas son: beautiful soup, scrapy y selenium.

Cualquiera de las tres herramientas se adapta para resolver la página de web propuesta, por lo que realicé una criba de las tres tecnologías. 
Scrapy es un framework lo cual implica que la manera de resolver el problema es muy concreta por lo que deja poco margen de maniobra. Selenium es una herramienta que te permite hacer pruebas con diferentes navegadores lo cual lo hace muy potente pero también muy pesado, por lo que finalmente me decidí por beautiful soup. Beautiful soup es una librería que nos brinda una serie de herramientas que nos permite trabajar de una manera cómoda con el html que le pasemos.

Como ya comenté en la primera entrevista, es la primera vez que utilizo el lenguaje de programación python y hago webscraping, por lo que la primera estrategia que decidí adoptar es resolver el ejercicio de la manera más simple posible, dando prioriad a entender como funciona python, identificar las diferentes librerías que serían necesarias y cómo implementarlas. Todo esto entendiendo que esta aproximación permitía hacerme una idea de cómo se resuelven ese tipo de problemas, identificar las diferentes partes y sus responsabilidades y de cómo se relacionan entre sí.

He decidido utilizar docker desde el principio para que no haya problemas entre las diferentes versiones de python que existen.

Como primera aproximación resolví la prueba en un solo archivo plano con las siguientes herramientas:
 - requests: para obtener el html de las páginas necesarias.
 - BeautifulSoup: para analizar y extraer información de documentos HTML y XML.
 - SoupStrainer: para que BeautifulSoup solo procese la parte que necesita del html, para mejorar el rendimiento.
Quedó pendiente la parte de crear el json y el guardado en un archivo, son dos tareas las cuales ya tenía más o menos claro como resolverlas y decidí ir al siguente paso ya que identifiqué que el acceso a las páginas tenía sentido encapsularlo.


---

En la siguiente iteración encapsulé la comunicación de los dos tipos de páginas, esto me permitía descargar el archivo main y encapsular el comportamiento especifico para cada página.

PagePrincipal: maneja la página principal.
PageContratos: maneja la páginas de contratos.
ListPageContratos: maneja las PageContratos.

En esta parte también implementé:
 - Un sistema de loguer rudimentario, como primera aproximación, ya que se comentó que estas aplicaciones se lanzan sin supervisión y me pareció interesante un sistema que registrara lo sucedido cada vez que se ejecute la aplicación. Ésto, conectado a un sistema de monitorización, nos permiten tanto la visualización como la creación de alertas en caso de fallos u otras circustancias que se consideren relevantes.
 - Incluí unos cuantos tests para no tener que hacer tantas pruebas al servidor real.
 - Cambié la estructura de carpetas. En esta parte tuve algunos problemas con la importación de los archivos al estar en diferentes carpetas que por suerte pude solucionar, igual no de la forma más elegante pero sí de una forma funcional.

Los tests que pude realizar no me permitían probar algunas partes de la aplicaión de manera individual, ésto me indicó que el código estaba muy acoplado entre sí, lo cual conlleva que sería difícil la modificación de algunas partes de la aplicación en caso de ser necesario.

En esta iteración entendí que la información de una página web es estructurada y que no difiere mucho de cómo se estrucutura en una base de datos, lo que sí cambia es la manera de como acceder a esos datos.

---
En la última iteración, después de lo aprendido en las anteriores iteraciones, y teniendo en cuenta la última reflexión, decidí darle más estructura.

El archivo **main.js**, como punto de entrada a la aplicación, se encargará de coordinar el flujo principal de la aplicación apoyándose a traves de **acciones**. Las **acciones** coordinarán los **servicios** que son los que resolverán las tareas específicas.

Con esta extructura los test son mas granulares y permiten testear de manera independiente las diferentes partes de la aplicación.

LLegado a este punto decidí crear dos entornos de desarrollo. Estos entornos están determinados a través de variables de entorno, que son **producción** y **test**. Ésto me permite, dependiendo del modo en el que se encuetre, que la aplicación realizará las peticiones al servidor o leer los archivos en local. También se podrían añadir más modos, como por ejemplo **dev**, que hicieran también las peticiones a los archivos en lugar de al servidor. También se podrían añadir otros comportamimientos, como por ejemplo si guardaramos datos, se podría determinar a qué base de datos hacerlo o lo guarde en un archivo.

Otro de los problemas que me topé es que no encontré cómo hacer espacios de nombre en python por lo que lo recurrí a una manera que vi hecha en javascript lo que me permitía dar más semántica al código y que sea más fácil determinar en qué parte de la estructura de la aplicación nos encontramos en cada momento.

Una cosa que queda pendiente es extraer del **main** el código que se encarga de generar el json y el gurdardado de los datos en un archivo, quedando de la siguente manera:

```js
catalog_name = 'Acuerdos año 2020'
contracts_list = Actions.ExtractContractsForCatalogName.do(catalog_name)
contracts_json = Actions.Convert.JSON.do(contracts_dictionary)
Action.Salve.JSON.do(contracts_json)
```

De esta manera, el flujo de la aplicación, queda mucho más claro permitiendo en una lectura rápida saber lo que hace la aplicaión y, por lo tanto, saber dónde actuar en caso de querer modificar alguna parte. 

---

Empecé la prueba con muchas dudas, al final he hecho un proceso iterativo añadiendo más estructura a medida que iba entendiendo el problema y las herramientas necesarias para resolverlos. He intentado enfocarlo sobre los problemas que se mencionaron en la primera entrevista que eran:

 - **El rendimiento** por eso utilizar `SoupStrainer` para procesar solo la parte necesaria del código.
 - **La observabilidad y la comunicación de errores** por lo que añadí los logs que nos permiten luego con otras herramientas conseguir este propósito, le faltaría un par de vueltas.
 - **La problemática de cuando cambia la estructura de la página web**, si el cambio no afecta a los datos que consumimos solo habría que actualizar el servicio que realizaba esa consulta a la página en concreto. Si el cambio es totalmente estructural, habría que cambiar los servicios afectados no afectando a las otras partes, como tratamiento, guardado de los datos...

La siguente cosa que me gustaría explorar es la extracción de datos con otra tecnología como pudiera ser Puppeteer, utilizando el código y la estructura hecha con python, esto lo haría separando la aplicación en dos partes: una con la lógica hecha en python y la otra de acceso a datos con Puppeteer.